LOCAL_PATH := $(call my-dir)

###############################################################################
# Prebuilt Launcher client library Libraries
#
include $(CLEAR_VARS)

LOCAL_MODULE := lib_launcherClient
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_SRC_FILES := libs/launcher_client.jar
LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_SDK_VERSION := 28

include $(BUILD_PREBUILT)

###############################################################################
# SearchLauncherQRef build rule (QSB, -1 Screen, Android Q Reference Nav)
#
include $(CLEAR_VARS)

# Relative path for Launcher3 directory
LAUNCHER_PATH := ../../../../packages/apps/Launcher3

LOCAL_STATIC_ANDROID_LIBRARIES := \
    Launcher3CommonDepsLib \
    SecondaryDisplayLauncherLib
LOCAL_STATIC_JAVA_LIBRARIES := lib_launcherClient

ifneq (,$(wildcard frameworks/base))
  LOCAL_STATIC_JAVA_LIBRARIES += SystemUISharedLib launcherprotosnano
  LOCAL_PRIVATE_PLATFORM_APIS := true
else
  LOCAL_STATIC_JAVA_LIBRARIES += libSharedSystemUI libLauncherProtos
  LOCAL_SDK_VERSION := system_current
  LOCAL_MIN_SDK_VERSION := 28
endif

LOCAL_SRC_FILES := \
    $(call all-java-files-under, quickstep/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/quickstep/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src_shortcuts_overrides) \
    $(call all-java-files-under, src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/src) \
    $(call all-java-files-under, $(LAUNCHER_PATH)/quickstep/recents_ui_overrides/src)

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/quickstep/res \
    $(LOCAL_PATH)/res \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/res \
    $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/recents_ui_overrides/res

LOCAL_PROGUARD_ENABLED := disabled

LOCAL_MODULE_TAGS := optional
LOCAL_USE_AAPT2 := true
LOCAL_AAPT2_ONLY := true
LOCAL_PACKAGE_NAME := SearchLauncherQRef
LOCAL_PRODUCT_MODULE := true
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Home Launcher2 Launcher3 Launcher3QuickStep SearchLauncher MtkLauncher3 MtkLauncher3QuickStep Op09Launcher3 Launcher3Go Launcher3NoQsb
LOCAL_REQUIRED_MODULES := privapp_permissions_searchlauncher SearchLauncherConfigOverlay
LOCAL_JACK_COVERAGE_INCLUDE_FILTER := com.android.launcher3.*

LOCAL_FULL_LIBS_MANIFEST_FILES := $(LOCAL_PATH)/$(LAUNCHER_PATH)/quickstep/AndroidManifest.xml

include $(BUILD_PACKAGE)

###############################################################################
include $(call all-makefiles-under,$(LOCAL_PATH))
